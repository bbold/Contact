//
//  Contacts.swift
//  Contact
//
//  Created by Mo Lotfi on 12/06/15.
//  Copyright (c) 2015 Mo Lotfi. All rights reserved.
//

import UIKit
//Class name should be singular

class Contacts: NSObject {
    var name : String?
    var number : String?
    var email : String?
    init(name: String? = nil, number: String? = nil, email: String? = nil) {
        self.name = name
        self.number = number
        self.email = email
    }
    
    
}
